import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class SpinkitDemo extends StatelessWidget {
  const SpinkitDemo({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SpinKitFadingCube(
        color: Colors.redAccent,
        size: 50.0,
        duration: Duration(seconds: 2),
      ),
    );
  }
}
