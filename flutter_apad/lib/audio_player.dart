import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';

class MyAudioPlayer extends StatelessWidget {
  const MyAudioPlayer({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text('Audio Player')),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              getButton("1", Colors.teal[100]),
              getButton("2", Colors.teal[200]),
              getButton("3", Colors.teal[300]),
              getButton("4", Colors.teal[400]),
              getButton("5", Colors.teal[500]),
              getButton("6", Colors.teal[600]),
              getButton("7", Colors.teal[700]),
            ],
          ),
        ),
      ),
    );
  }

  Widget getButton(String no, Color? warna) {
    return ElevatedButton(
      onPressed: () async {
        final player = AudioPlayer();
        await player.setSource(AssetSource('assets_note$no.wav'));
        await player.resume();
      },
      style: ElevatedButton.styleFrom(backgroundColor: warna),
      child: Text('Button $no'),
    );
  }
}
