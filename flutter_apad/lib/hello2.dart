import 'package:flutter/material.dart';

class Hello2 extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.grey,
        appBar: AppBar(
          title: Text('Hello'),
          centerTitle: true,
          backgroundColor: Colors.amber,
        ),
        body: Center(
          child: Image(
            image: NetworkImage('http://jomdemy.com/images/java1.png'),
            width: 200.0,
          ),
        ),
      ),
    );
  }
}
