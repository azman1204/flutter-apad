import 'package:flutter/material.dart';
import 'package:flutter_apad/audio_player.dart';
import 'package:flutter_apad/bizcard.dart';
import 'package:flutter_apad/dadu.dart';
import 'package:flutter_apad/home2.dart';
import 'package:flutter_apad/home3.dart';
import 'package:flutter_apad/list1.dart';
import 'package:flutter_apad/post.dart';
import 'package:flutter_apad/spinkit.dart';

void main() {
  runApp(MaterialApp(
    initialRoute: '/home3',
    routes: {
      '/bizcard' : (context) => Bizcard(),
      '/dadu' : (context) => Dadu(),
      '/audio' : (context) => MyAudioPlayer(),
      '/list1' : (context) => List1(),
      '/posting': (context) => Post(),
      '/spin': (context) => SpinkitDemo(),
      '/home': (context) => Home2(),
      '/home3': (context) => Home3(),
    },
    // home: Scaffold(
    //   body: Home2(),
    // ),
  ));
}