class Person {
  // ? = nullable
  String? firstName;
  String? lastName;
  String getFullName() => "$firstName $lastName"; // arrow function
  //  ini ada lah shortcut kpd
  // String getFullName() {
  //   return "$firstName $lastName";
  // }
}

class Student {
  String? name;
  // constructor with named parameter {}
  Student({String? name}) {
    this.name = name;
  }
}

main() {
  Student stu = Student(name: 'Azman');
  // create obj dari class.. instantiate
  Person p = Person();
  p.firstName = 'John';
  p.lastName = 'Doe';
  print(p.getFullName());
}