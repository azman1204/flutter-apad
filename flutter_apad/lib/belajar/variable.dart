void main() {
  print("Hello World");
  String name = "Azman";
  print("Nama saya " + name);
  print("Nama saya ${name}");

  int age = 40;
  print(age + 5);

  double salary = 5500.50;
  print(salary + 125.00);

  bool isMarried = true;
  print(isMarried);

  dynamic number = 100;
  print("number = $number");

  number = "seratus";
  print("number = $number");
}