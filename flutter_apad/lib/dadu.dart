import 'package:flutter/material.dart';
import 'dart:math';

class Dadu extends StatefulWidget {
  const Dadu({super.key});

  @override
  State<Dadu> createState() => _DaduState();
}

class _DaduState extends State<Dadu> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.teal,
        appBar: AppBar(
          title: Text('Lambung Dadu'),
          backgroundColor: Colors.teal[700],
        ),
        body: Dadu2(),
      ),
    );
  }
}

class Dadu2 extends StatefulWidget {
  const Dadu2({super.key});

  @override
  State<Dadu2> createState() => _Dadu2State();
}

class _Dadu2State extends State<Dadu2> {
  int dadu1 = 1;
  int dadu2 = 1;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        children: [
          Expanded(
            child: TextButton(
              onPressed: (){
                setState(() {
                  dadu1 = Random().nextInt(6) + 1;
                  dadu2 = Random().nextInt(6) + 1;
                  print("dadu1 = $dadu1 dadu2 = $dadu2");
                });
              },
              child: Image(
                image: AssetImage('assets/dice$dadu1.png'),
              ),
            ),
          ),
          Expanded(
            child: TextButton(
              onPressed: (){
                print('you click me');
                setState(() {
                  dadu1 = Random().nextInt(6) + 1;
                  dadu2 = Random().nextInt(6) + 1;
                  print("dadu1 = $dadu1 dadu2 = $dadu2");
                });
              },
              child: Image(
                image: AssetImage('assets/dice$dadu2.png'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
