import 'package:flutter/material.dart';

class List1 extends StatelessWidget {
  const List1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.teal,
      body: ListView(children: hikmah2(context)),
    );
  }

  List<Widget> hikmah() {
    List quotes = [
      'Hari ini lebih baik dari hari semalam',
      'Hari ini lebih baik dari hari semalam',
      'Hari ini lebih baik dari hari semalam',
      'Hari ini lebih baik dari hari semalam',
      'Hari ini lebih baik dari hari semalam',
      'Hari ini lebih baik dari hari semalam',
    ];
    return quotes.map((data) =>
        Container(
          color: Colors.teal,
          height: 150.0,
          padding: EdgeInsets.all(8.0),
          margin: EdgeInsets.all(8.0),
          child: Text(
            data,
            // style: TextStyle(
            //   fontSize: 14.0,
            //   color:Colors.white,
            //   decoration: TextDecoration.none
            // ),
          ),
        )).toList();
  }

  List<Widget> hikmah2(context) {
    List quotes = [
      {'title': 'Hari ini lebih baik dari hari semalam', 'sub': 'Azman'},
      {'title': 'Hari ini lebih baik dari hari kelmarin', 'sub': 'Abu'},
      {'title': 'Hari ini lebih baik dari hari minggu lepas', 'sub': 'John Doe'},
    ];
    return quotes.map((data) =>
        Card(
          child: ListTile(
            title: Text(data['title']),
            subtitle: Text('By: ${data['sub']}'),
            leading: Icon(Icons.add_circle),
            trailing: Icon(Icons.add_alarm_outlined),
            onTap: () {
              Navigator.pop(context);
            },
          ),
        )
    ).toList();
  }
}
