import 'package:flutter/material.dart';

class Mylayout extends StatelessWidget {
  const Mylayout({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        child: Scaffold(
          body: Row(
            children: [
              Text('John Doe'),
              Text('Abu Bakar'),
              Text('Mohd Ellah'),
              Column(
                children: [
                  TextButton(
                      onPressed: (){},
                      child: Text("Click")
                  ),
                  IconButton(
                      onPressed: (){},
                      icon: Icon(Icons.add_circle)
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

