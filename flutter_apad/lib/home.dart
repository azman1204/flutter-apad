import 'package:flutter/material.dart';
import 'package:flutter_apad/audio_player.dart';
import 'package:flutter_apad/bizcard.dart';
import 'package:flutter_apad/dadu.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(width: double.infinity,),
          ElevatedButton(
            onPressed: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) {
                return Bizcard();
              }));
            },
            child: Text('Bizcard'),
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return Dadu();
              }));
            },
            child: Text('Lambung Dadu'),
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return MyAudioPlayer();
              }));
            },
            child: Text('Audio Player'),
          ),
        ],
      ),
    );
  }
}
