import 'package:flutter/material.dart';

class Bizcard extends StatelessWidget {
  const Bizcard({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.teal[500],
          body: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CircleAvatar(
                  backgroundImage: AssetImage('assets/sanusi.jpg'),
                  radius: 50,
                ),
                Text(
                  'Sanusi Mohd Nor',
                  style: TextStyle(
                    fontSize: 35.0,
                    color: Colors.white,
                    fontFamily: 'IndieFlower',
                  ),
                ),
                Text('IT Trainer', style: TextStyle(fontSize: 25, color: Colors.white),),
                SizedBox(
                  width: double.infinity,
                  child: Divider(color: Colors.white,),
                ),
                Card(
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Row(
                      children: [
                        Icon(Icons.email, color: Colors.teal,),
                        SizedBox(width: 10.0),
                        Text('azman1204@yahoo.com')
                      ],
                    ),
                  ),
                ),
                Card(
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Row(
                      children: [
                        Icon(Icons.phone, color: Colors.teal,),
                        SizedBox(width: 10.0),
                        Text('016 2370 394')
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
