import 'package:flutter/material.dart';
import 'package:flutter_apad/audio_player.dart';
import 'package:flutter_apad/bizcard.dart';
import 'package:flutter_apad/dadu.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Home2 extends StatelessWidget {
  const Home2({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(width: double.infinity,),
          ElevatedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/bizcard');
            },
            child: Text('Bizcard'),
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/dadu');
            },
            child: Text('Lambung Dadu'),
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/audio');
            },
            child: Text('Audio Player'),
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/list1');
            },
            child: Text('Quote List'),
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/posting');
            },
            child: Text('Posting'),
          ),
        ],
      ),
    );
  }
}
