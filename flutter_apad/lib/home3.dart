import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Home3 extends StatefulWidget {
  const Home3({super.key});

  @override
  State<Home3> createState() => _Home3State();
}

class _Home3State extends State<Home3> {
  @override
  void initState() {
    super.initState();
    tidur();
  }

  void tidur() async {
    // sleep 5 seconds.. then redirect
    await Future.delayed(Duration(seconds: 5));
    Navigator.pushNamed(context, '/home');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SpinKitFadingCube(
        color: Colors.redAccent,
        size: 50.0,
        duration: Duration(seconds: 2),
      ),
    );
  }
}
