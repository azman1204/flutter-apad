import 'package:flutter/material.dart';
import 'package:flutter_apad/home.dart';

void main() {
  runApp(MaterialApp(
    theme: ThemeData(
      useMaterial3: false,
      colorScheme: ColorScheme.fromSeed(
        seedColor: Colors.teal,
        brightness: Brightness.dark,
      ),
    ),
    home: Scaffold(
      appBar: AppBar(title: Text('Home Screen'),),
      body: Home(),
    ),
  ));
}