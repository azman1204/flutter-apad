import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Post extends StatefulWidget {
  const Post({super.key});
  @override
  State<Post> createState() => _PostState();
}

class _PostState extends State<Post> {
  List posts = [];
  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() async {
    var url = Uri.https('jsonplaceholder.typicode.com', 'posts');
    var response = await http.get(url);
    setState(() {
      //print(response.body);
      this.posts = json.decode(response.body);
      print(this.posts[0]['title']);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.teal,
      appBar: AppBar(title: Text('Article Posting')),
      body: ListView(
          children: this.posts.map((data) =>
              Card(
                child: ListTile(
                  title: Text(data['title']),
                  subtitle: Text(data['body']),
                ),
              )
          ).toList(),
      )
    );
  }
}